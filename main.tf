provider "aws" {
  max_retries = 2
  region      = "us-east-1"
  profile     = "default"
}

terraform {
  backend "s3" {}
}


data "aws_ami" "vault" {
  most_recent = true

  filter {
    name   = "name"
    values = ["vault"]
  }

  owners = ["747313434235"] # Canonical
}

resource "aws_default_vpc" "default" {
  tags = {
    Name = "Default VPC"
  }
}

data "aws_subnet_ids" "default_us_east_1a" {
  vpc_id = aws_default_vpc.default.id

  filter {
    name   = "tag:Name"
    values = ["Default subnet for us-east-1a"]
  }
}

resource "aws_security_group" "vault_security_group" {
  name        = "vault_security_group"
  description = "Security Group Rules for our Vault Cloud instance"
  vpc_id      = aws_default_vpc.default.id

  ingress = [
    {
      description = "SSH from my computer"
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      cidr_blocks = [
        "66.216.235.40/32"
      ]
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      security_groups  = []
      self             = false
    }
  ]

  egress = [
    {
      description      = ""
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
      prefix_list_ids  = []
      security_groups  = []
      self             = false
    }
  ]

  tags = {
    Name = "allow_tls"
  }
}

# Create Assume Role Policy
data "aws_iam_policy_document" "assume_role_policy" {
  statement {
    actions = [
      "sts:AssumeRole",
    ]

    principals {
      type = "Service"
      identifiers = [
        "ec2.amazonaws.com",
      ]
    }
  }
}

resource "aws_iam_role" "ec2_role" {
  name               = "vault-ec2-role"
  description        = "Vault server ec2 role"
  assume_role_policy = data.aws_iam_policy_document.assume_role_policy.json

  tags = {
    Name = "vault-ec2-role"
  }
}

resource "aws_iam_instance_profile" "ec2_instance_profile" {
  name = "vault-ec2-instance-profile"
  role = aws_iam_role.ec2_role.name
}


resource "aws_instance" "web" {
  ami                  = data.aws_ami.vault.id
  instance_type        = "t2.micro"
  availability_zone    = "us-east-1a"
  key_name             = "personalPCKey.pem"
  iam_instance_profile = aws_iam_instance_profile.ec2_instance_profile.name

  security_groups = [
    aws_security_group.vault_security_group.id
  ]

  tags = {
    Name = "Vault Server"
  }
}
